const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    
    const navlinks = document.querySelectorAll('nav-links li');
    
    //toogle
    burger.addEventListener('click', () => {
        nav.classList.toggle('nav-activate');
    });
    
    //animation
    navlinks.forEach((link,index)=>{
        link.style.animation = 'navLinkFade 0.5s ease forwards ${index / 7 + 1.5}s';
    }); 
}

navSlide();