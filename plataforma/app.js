'use strict';

var express = require('express');
var bodyParser= require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//cargar rutas
var minuteRoutes = require('./routes/minutes');

app.use('/minutes',minuteRoutes);

module.exports = app;