'use strict';

var Minute = require('../model/minute');
var mongoose = require('mongoose');

function test(req,res){
    res.status(200).send({message:'Mensaje desde controlador'});
}

function saveMinute(req,res){
        var minute = new minute();
        var params = req.body;

        minute.accountNo    =   params.accountNo;
        minute.name         =   params.name;
        minute.minuteNo     =   params.minuteNo;
        minute.Notification =   params.Notification;
        minute.minuteDesc   =   params.minuteDesc;

        minute.save((err, savedMinute)=>{
            if(err){
                res.status(500).send({message: 'There is an error on the server'});
            }else{
                if(!saveMinute){
                    res.status(404).send({message: 'Incomplete information'});
                }else{
                    res.status(200).send({minute:savedMinute});
                }
            }
        });
}

function deleteMinute(req,res){
    let MinuteId= req.params.id;
    Minute.findByIdAndRemove(MinuteId,(err,deletedMinute)=>{
       if(err){
        res.status(500).send({message: 'There is an error on the server'});
       }else{
            if(!deletedMinute){
                res.status(404).send({message: 'The Minute Does not exists'});
            }else{
                res.status(200).send({minute:deletedMinute});
            }
        }
    });
}

function updateMinute(req,res){
    let MinuteId= req.params.id;
    var params = req.body;

    Minute.findByIdAndUpdate(MinuteId,params,(err,updatedMinute)=>{
       if(err){
        res.status(500).send({message: 'There is an error on the server'});
       }else{
            if(!updatedMinute){
                res.status(404).send({message: 'The Minute Does not exists'});
            }else{
                res.status(200).send({minute:updatedMinute});
            }
        }
    });
}

function getMinutes(req,res){
    Minute.find((err,minutes)=>{
        if(err){
            res.status(500).send({message: 'There is an error on the server'});
        }else{
            if(!updatedMinute){
                res.status(404).send({message: 'There is no information'});
            }else{
                res.status(200).send({minutes:minutes});
            }
        } 
    });
}


module.exports = {
    test,
    saveMinute,
    deleteMinute,
    getMinutes,
    updateMinute
};