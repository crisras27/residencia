'use strict';

var mongoose = require('mongoose');
var app = require('./app');

const PORT = 3000;

mongoose.connect('mongodb://localhost:27017/minutes').then(()=>{
    app.listen(PORT,()=>{
        console.log('El servidor web esta en linea');
    });
    console.log('el DBMS esta en linea');
}).catch(err=>console.log(err));