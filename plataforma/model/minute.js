'use strict';

var mongoose = require('mongoose');

var schema = mongoose.Schema;

var Minute = schema({
    accountNo   : Number    ,
    name        : String    ,
    minuteNo    : Number    ,
    Notification: String    ,
    minuteDesc  : String    ,
});

module.exports = mongoose.model('minute',Minute);