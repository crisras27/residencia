'use strict';

var express = require('express');

var api = express.Router();

var minuteController = require('../control/minutes');

api.post('/minute',minuteController);
api.get('/minute',minuteController.test);
api.put('/minute/:id',minuteController);
api.delete('/minute/:id',minuteController);

module.exports=api;